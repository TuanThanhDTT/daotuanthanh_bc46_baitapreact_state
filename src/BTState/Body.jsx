import React from 'react'

const Body = (props) => {
    const {data, img, handleChangeGlasses} = props
  return (
    <div className='body'>
        <h1>Welcome to your trial of the glasses</h1>
        <div>
            <div className="modelCard" style={{width: 400}}>
                <img className="card-img" src="./images/glasses/model.jpg" alt="Card image" />
                <img className='model-glasses' src={img.url} alt="" />
                <div className="text-left img-content">
                    <h4 className="card-title">{img.name}</h4>
                    <p className="card-text">{img.desc}</p>
                </div>
            </div>
            <div className='product-glasses row'>
            {
                    data.map((item)=> {
                        return ( 
                            <div key={item.id} className='product1 col-2 mt-3'>
                                 <a href="#" onClick={()=>{handleChangeGlasses(item)}}>
                                    <img src={item.url} alt="" className='img-fluid' />
                                </a>
                            </div>
                        )
                    })
                }           
            </div>
        </div>
    </div>
  )
}

export default Body