import React, { useState } from 'react'
import Header from './Header'
import './style.scss'
import Body from './Body'
import data from './data.json'
const BTState = () => {
    const [img, setImg] = useState(data[0])
    const handleChangeGlasses = (img) => {
        setImg(img)
    }
  return (
    <div className='BTState'>
        <div className='mainHeader'>
            <Header/>
        </div>
        <div className="mainBody text-center">
            <Body data={data} img={img} handleChangeGlasses={handleChangeGlasses}/>
        </div>
        <br/>
        <br/>
    </div>
  )
}

export default BTState